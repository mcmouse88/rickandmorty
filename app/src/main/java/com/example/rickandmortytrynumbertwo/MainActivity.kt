package com.example.rickandmortytrynumbertwo

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.example.rickandmortytrynumbertwo.databinding.ActivityMainBinding
import com.example.rickandmortytrynumbertwo.epoxy.CharacterDetailEpoxyController


class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private val viewModel: SharedViewModel by lazy {
        ViewModelProvider(this)[SharedViewModel::class.java]
    }
    private val epoxyController = CharacterDetailEpoxyController()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        viewModel.characterByIdLiveData.observe(this) {
            epoxyController.characterResponse = it
            if (it == null) {
                Toast.makeText(this, "Unsuccessful network call", Toast.LENGTH_SHORT).show()
                return@observe
            }
        }
        viewModel.refreshCharacter(54)
        val epoxyRecyclerView = binding.epoxyRecyclerView
        epoxyRecyclerView.setControllerAndBuildModels(epoxyController)
    }
}