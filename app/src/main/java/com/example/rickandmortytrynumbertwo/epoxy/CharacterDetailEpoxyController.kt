package com.example.rickandmortytrynumbertwo.epoxy

import com.airbnb.epoxy.EpoxyController
import com.example.rickandmortytrynumbertwo.network.response.GetCharacterByIdResponse
import com.example.rickandmortytrynumbertwo.R
import com.example.rickandmortytrynumbertwo.databinding.ModelCharacterDataPointBinding
import com.example.rickandmortytrynumbertwo.databinding.ModelCharacterDetailHeaderBinding
import com.example.rickandmortytrynumbertwo.databinding.ModelCharacterDetailImageBinding
import com.squareup.picasso.Picasso

class CharacterDetailEpoxyController : EpoxyController() {

    var isLoading: Boolean = true
        set(value) {
            field = value
            if (field) {
                requestModelBuild()
            }
        }

    var characterResponse: GetCharacterByIdResponse? = null
        set(value) {
            field = value
            if (field != null) {
                isLoading = false
                requestModelBuild()
            }
        }

    override fun buildModels() {
        if (isLoading) {
            LoadingEpoxyModel().id("loading").addTo(this)
            return
        }

        if(characterResponse == null) {
            return
        }

        // add header model
        HeaderEpoxyModel(
            name = characterResponse!!.name,
            gender = characterResponse!!.gender,
            status = characterResponse!!.status
        ).id("header").addTo(this)
        // add image model
        ImageUrl(
            image = characterResponse!!.image
        ).id("image").addTo(this)
        // add data point
        DataPointEpoxyModel(
            origin = characterResponse!!.origin.name,
            species = characterResponse!!.species
        ).id("data").addTo(this)
    }

    data class HeaderEpoxyModel(
        val name: String,
        val gender: String,
        val status: String
    ) : ViewBindingKotlinModel<ModelCharacterDetailHeaderBinding>(R.layout.model_character_detail_header) {
        override fun ModelCharacterDetailHeaderBinding.bind() {
            nameView.text = name
            statusView.text = status
            if (gender.equals("male", true)) {
                genderView.setImageResource(R.drawable.ic_male_gender)
            } else genderView.setImageResource(R.drawable.ic_female)
        }
    }

    data class ImageUrl(
        val image: String
    ) : ViewBindingKotlinModel<ModelCharacterDetailImageBinding>(R.layout.model_character_detail_image) {
        override fun ModelCharacterDetailImageBinding.bind() {
            Picasso.get().load(image).into(avatar)
        }
    }

    data class DataPointEpoxyModel(
        val origin: String,
        val species: String
    ) : ViewBindingKotlinModel<ModelCharacterDataPointBinding>(R.layout.model_character_data_point) {
        override fun ModelCharacterDataPointBinding.bind() {
            originView.text = origin
            speciesView.text = species
        }
    }
}