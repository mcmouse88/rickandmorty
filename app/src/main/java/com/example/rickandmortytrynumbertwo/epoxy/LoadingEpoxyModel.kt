package com.example.rickandmortytrynumbertwo.epoxy


import com.example.rickandmortytrynumbertwo.R
import com.example.rickandmortytrynumbertwo.databinding.ContentLoadingProgressBarBinding

class LoadingEpoxyModel : ViewBindingKotlinModel<ContentLoadingProgressBarBinding>(R.layout.content_loading_progress_bar) {

    override fun ContentLoadingProgressBarBinding.bind() {
        // noting to do
    }
}