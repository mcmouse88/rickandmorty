package com.example.rickandmortytrynumbertwo

import com.example.rickandmortytrynumbertwo.network.NetWorkLayer
import com.example.rickandmortytrynumbertwo.network.response.GetCharacterByIdResponse

class SharedRepository {
    suspend fun getCharacterById(characterId: Int): GetCharacterByIdResponse? {
        val request = NetWorkLayer.apiClient.getCharacterById(characterId)

        if (request.failed) return null

        if (!request.isSuccessful) return null

        return request.body
    }
}