package com.example.rickandmortytrynumbertwo.network.response

data class Location(
    val name: String,
    val url: String
)