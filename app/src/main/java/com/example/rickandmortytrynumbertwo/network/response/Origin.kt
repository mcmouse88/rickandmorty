package com.example.rickandmortytrynumbertwo.network.response

data class Origin(
    val name: String,
    val url: String
)